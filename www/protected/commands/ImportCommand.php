<?php
// ini_set('max_execution_time', 300);
class ImportCommand extends CConsoleCommand {
    
	public function run($args) {
				
		$url = 'http://test1.ru/etagi.json';
		$url = 'https://krasnodar.etagi.com/backend/?action=get_builder_objects&builder_id=1046';
				
		$content = file_get_contents($url);
			
		$array_data = json_decode($content);
		
		if ($array_data) {
		    Flats::model()->deleteAll();
		}
		
		foreach ($array_data->objects AS $object) {			
			
			if ($building = Buildings::model()->findByAttributes(array('import_id'=>'etagi_'.$object->id))) {
				
				//берем страницу для вытаскиваня планировок
				$page_content = file_get_contents('https://krasnodar.etagi.com/zastr/jk/none-'.$object->id);
				
				//бегу по квартирам
				foreach ($object->flats as $object_flat) {
					
					$square_kitchen = floatval($object_flat->square_kitchen);
					
					$rooms = intval($object_flat->rooms);
					if ($square_kitchen==0 AND $rooms==1) {
					    $rooms = 0;
					}
										
					$plan_params = array(
							'buildingid'=>$building->id,
							'rooms'=>$rooms,
							's_total'=>floatval($object_flat->square),
							's_kitchen'=>$square_kitchen,
							's_living'=>0,
					);
					
					$imageplan = '';
							
					//если такой планировки нет
					if (!$plan = Plans::model()->findByAttributes($plan_params)) {
					    
					    //создаю планировку
					    $plan = new Plans();
					    $plan->attributes = $plan_params;
					    	
					    //беру инфу о квартире на сайте этажей
					    if (preg_match('/"id":"'.$object_flat->object_id.'".*?"layout2d":"(.*?)"/',$page_content, $match)) {
					    
					        $imageplan = $match[1].'?cache='.time();
					        
					        $plan->addImage = $imageplan;
					    }					
						
					    $plan->save();
												
					}
					
					if ($plan) {
					    
					    
					    /*
					    echo $plan->id;
					    echo PHP_EOL;
					    //check image
					    if (!getimagesize(Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.$plan->getImgSrc())) {
					        
					        echo $plan->addImage = $imageplan;
					        $plan->save();
					    }
					    */
					    
					    $hot = 0;
					    if ($object_flat->old_price>0 && $object_flat->old_price>$object_flat->price) {
					        $hot = 1;
					    }
					    
					    $flat = new Flats();					    
					    $flat->attributes = array(
	                        'planid' => $plan->id,
	                        'floor' => $object_flat->floor,
	                        'price' => $object_flat->price,
					        'hot' => $hot,
	                    );	                   
	                   $flat->save();					    
					}					
				}				
			}			
		}
	}
}