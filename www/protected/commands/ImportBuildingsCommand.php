<?php
// ini_set('max_execution_time', 300);
class ImportBuildingsCommand extends CConsoleCommand
{

    public function run($args)
    {
        $url = 'http://test1.ru/etagi.json';
        $url = 'https://krasnodar.etagi.com/backend/?action=get_builder_objects&builder_id=1046&cache=1';
        
        $content = file_get_contents($url);
        
        $array_data = json_decode($content);
        
        $conformity = array(
            159832 => 16,
            109305 => 13,
            109885 => 13,
            109309 => 13,
            109352 => 14,
            88495 => 14,
            147879=> 14,
            103565 => 15,
            108870 => 15,
            109283 => 12,
            135603 => 12,
            120791 => 12,
            137253 => 12,
            95451 => 11,
            123346 => 11,
            95496 => 17,            
        );
        
        $intToRimsk = array(1=>'I',2=>'II',3=>'III',4=>'IV');
        
        foreach ($array_data->objects as $object) {
            
            if (! ($building = Buildings::model()->findByAttributes(array('import_id' => 'etagi_' . $object->id))) 
                && $sectionid = @$conformity[$object->id]) {
                
                $building = new Buildings();
                
                $building->attributes = array(
                    'sectionid'=>$sectionid,
                    'name'=>$object->gp,
                    'material'=>$object->walls,
                    'adress'=>'г. Краснодар, '.$object->street.' '.$object->house,
                    'import_id'=>'etagi_'.$object->id,
                    'floors'=>$object->info->floors,
                    'complTime'=>$intToRimsk[$object->deadline_q].' '.$object->deadline_y,
                );
                
                $building->save();
                
                echo 11;
            }
        }
    }
}