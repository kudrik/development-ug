<?php
$this->pageTitle='Новости - '.Yii::app()->name;
?>
<h1 class="e-h1_pad">Новости</h1>

<div class="b-news">

	<?php
	foreach ($items as $item)
	{
		?>
		<table class="e-row">
		<tr>
			<td>
			 	<div class="e-name">
			 		<?php			 		
			 		$link='';
			 		if ($item->tekst<>'') { $link=$this->createUrl('/news/item',array('id'=>$item->id)); }
			 		
			 		if ($link<>'') 	{ ?><a href="<?php echo $link;?>"><?php echo $item->name;?></a><?php }
			 		else			{ echo $item->name; } 
			 		?>			 		
			 	</div>
				
				<div class="e-desc"><?php echo $item->anons;?></div>
										
				<span class="e-date">30.10.2013</span>
				<?php if ($link<>'') { ?><a href="<?php echo $link;?>" class="e-adv">Подробне...</a><?php } ?>
		  </td>
		</tr>
		</table>
		<?php
	}
	?>
</div>


