<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css?v=3" />
	
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	
	<?php
	Yii::app()->getClientScript()->registerCoreScript('jquery');
	
	Yii::app()->clientScript->registerScriptFile('/js/index.js?v=1');
	
	//подключаю lightbox
	//if (substr_count($content,'lightbox'))
	{
		Yii::app()->clientScript->registerScriptFile('/js/lightbox/js/jquery.lightbox-0.5.js');
		Yii::app()->getClientScript()->registerCssFile('/js/lightbox/css/jquery.lightbox-0.5.css');
		Yii::app()->clientScript->registerScript('lightbox',"$(function() {	$('a.lightbox').lightBox(); });");
	}
	?>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>


<table class="b-shell <?php if ($this->is_main_page) { echo 'b-head_main'; } else { echo 'b-head_std'; } ?>">
<tr>
  <td class="b-head">
  	<div class="b_shell_head">
	
		<a href="/" class="b-logo" title="На главную">Девелопмент-Юг <span class="e-small">строительная компания</span></a>		
		
		<a href="<?php echo $this->createUrl('orders/callme');?>" class="b-tel_but"  onclick="get_url_to_modal_windows(this.href); return false; ">Заказать звонок</a>		
				
		<div class="b-tel">
			<div class="e-number" id="phoneNumber">+7 (861) 244-12-99</div>
			<div class="e-hours">пн-вс 9:00-21:00</div>			
		</div>
		
		<div class="b-bus"><a href="<?php echo $this->createUrl('orders/excursions');?>" onclick="get_url_to_modal_windows(this.href); return false; ">Экскурсии</a><br>по новостройкам</div>
				
		<div class=clear></div>		

		<div class="b-menu">
			
			<?php
			
			if ($this->complexPage) {
			    ?>
    			<a href="#about" class="e-menu" >О жилом комплексе</a>
    			<a href="#flats" class="e-menu" >Планировки и цены</a>
    			<a href="#ipoteka" class="e-menu" >Ипотека</a>
    			<a href="/#complexes" class="e-menu" >Все жилые комплексы</a>
    			<?php 
			} else {
			    ?>
			    <a href="/#complexes" class="e-menu" >Жилые комплексы</a>
			    <?php 
			}
			
			

			foreach (Section::menu() as $menu) {
			    
			    if ($this->complexPage AND $menu['url']=='ipoteka') {
			        continue;
			    }
				?>
				<a href="<?php if ($menu['anchor']) { echo '/#'.$menu['url']; } else { echo $this->createUrl('site/page',array('id'=>$menu['id'])); } ?>" class="e-menu" ><?php echo $menu['name'];?></a>
				<?php
			}			
			?>
			<div class="e-partner"></div>	
			<div class="clear"></div>	
		</div>
	</div>
  </td>
</tr>	
<tr>
  <td class="b-main">
	<div class="b-area_cont">
		<?php echo $content; ?>
	</div>	
  </td>
</tr>
<tr>
  <td class="b-footer">
  				
	<div class="b-backToTop" title="на верх"></div>		
  				
	<table style="width:100%;">
	<tr>
	  <td style="width:15%;">
	  	<a class="e-menu" href="<?php echo $this->createUrl('index/');?>">Главная</a>
	  <a class="e-menu" href="/privacy">Соглашение об обработке персональных данных</a>
	  	
	  </td>
	  <td style=" padding:0 5%;">
	  
	  	<p>Сайт носит исключительно информационный характер и никакая информация, опубликованная на нём, ни при каких условиях не является публичной офертой. Для получения подробной информации о реализуемых товарах, работах и услугах и их цене необходимо обращаться по указанному в контактах телефону или оставить заявку на сайте.</p>
	  	
	  
	  </td>
	  				 
	  <td style="width:30%;">
		<p>&copy; 2016 <span class="b-etagi" title="Этажи"></span> &nbsp; Все Права Защищены</p>
		<p>Официальный партнер застройщика</p>	
		<p>Северная 311/1, телефон: +7 (861) 244-12-99</p>
		<p>E-mail: <a href="mailto:<?php echo Yii::app()->params['email'];?>"><?php echo Yii::app()->params['email'];?></a>, Создание сайта: <a href="http://vk.com/public85884884" target="sib promo">Sib Promo</a></p>
		  
	  </td>
	</tr>  			
	</table>
  </td>
</tr>
</table>

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40731739 = new Ya.Metrika({ id:40731739, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/40731739" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<!--  php developer Plotkin Konstantin 12.2016 -->
</body>
</html>