<?php
if ($page->show_tit==1) { $this->h1=$page->name; }
?>
<p>Адрес: <?php echo $page->anons; ?></p>
<div class="b-promo" style="background-image: url('/images/promo/<?php echo $page->id; ?>.jpg');"></div>

<div id="about">
<?php echo $page->tekst; ?>
</div>
<div class="clear"></div>
<?php 
$fotos = $page->foto;
if (count($fotos)>0) {
    ?>
    <h2 id="foto">Фото <?php echo $page->name; ?></h2>
    <?php
    $this->renderPartial('//foto/index',array('items'=>$fotos));
}
?>
<h2 id="flats">Планировки и цены</h2>
<p style="font-size:0.9em;">Вы можете забронировать квартиры прямо у нас на сайте. После подтверждения заявки бронь сохраняется в течении 5 дней.</p>
<?php
$this->renderPartial('//flats/index',array('buildings'=>$page->buildings,'complexName'=>$page->name));
?>
<p>&nbsp;</p>
<h2>Карта <?php echo $page->name; ?></h2>
<img src="/images/maps/<?php echo $page->id; ?>.jpg" style="max-width:100%;" alt="Карта <?php echo $page->name; ?>">
<h2 id="ipoteka">Как купить квартиру в «Девелопмент-Юг»</h2>
<?php 
$ipoteka = Section::model()->findByAttributes(array('url'=>'ipoteka','level'=>1));

echo $ipoteka->tekst;

?>
<h3 id="complexes">Жилые комплексы от «ДЕВЕЛОПМЕНТ-ЮГ»</h3>
<?php
$this->renderPartial('//complex/_list',array('items'=>$complexes));
