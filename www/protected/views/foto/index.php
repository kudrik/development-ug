<?php 
if (count($items)>0)
{
	?>
	<div class="b-fotos">
		<?php
		$className='lightboxFoto'.$items[0]->pid;
		
		Yii::app()->clientScript->registerScript($className,"$(function() {	$('a.".$className."').lightBox(); });");
		
		foreach ($items AS $foto)
		{
			?>
			<a class="e-row <?php echo $className;?>" href="<?php echo $foto->img;?>" title="<?php echo CHtml::encode($foto->name);?>" style="background-image:url('<?php echo $foto->imgM;?>');"></a>
			<?php
		}
		?>
		<div class=clear></div>
	</div>
<?php 
}