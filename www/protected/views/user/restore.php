<?php
$this->h1='';
?>

<p style="float:right; margin-top:14px;"><a href="<?php echo $this->createUrl('user/profile');?>" onclick="get_url_to_modal_windows(this.href); return false;">Регистрация</a></p>

<h1>Восстановление пароля</h1>

<div class="b-form" style="width:250px;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>


	<div class="e-row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	

	<div class="e-row buttons">
		<?php echo CHtml::submitButton('Восстановить'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
