<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	
	<?php
	Yii::app()->getClientScript()->registerCoreScript('jquery');
	
	Yii::app()->clientScript->registerScriptFile('/js/index.js');
	?>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body style="background:none;">

<?php 
if (Yii::app()->user->isGuest)
{
	?>
	<p>Если вы раньше заказывали или регистрировались у нас, <a href="<?php echo $this->createUrl('user/login');?>"  onclick="get_url_to_modal_windows(this.href); return false; ">войдите</a> используя логин и пароль.</p>
	<?php 
}
?>

<?php echo $content; ?>
</body>
</html>