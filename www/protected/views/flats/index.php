<?php
Yii::app()->clientScript->registerScript('hottableexpand','tableExpander("#flatTable",5,"Смотреть все &raquo;"); ');

foreach ($buildings as $building) {
    
	$lightbox='lightboxFlats'.$building->id;
	
	Yii::app()->clientScript->registerScript($lightbox,"$(function() {	$('a.".$lightbox."').lightBox(); });");
		
	?>
	<div class="b-dom" id="building<?php echo $building->id;?>">
		<a href="/images/buildings/<?php echo $building->id;?>.jpg" class="e-img <?php echo $lightbox;?>" style="background-image:url('/images/buildings/s<?php echo $building->id;?>.jpg'); " title="<?php echo CHtml::encode($building->name.', '.$building->adress);?>"></a>
		<div><span class="e-name"><?php echo $building->name;?></span> <span style="font-size:0.8em;"><?php echo $building->adress;?></span></div>				
		<div style="font-size:0.8em;"><?php echo $building->complTime;?></div>
		<div style="font-size:0.8em;"><?php echo $building->getAttributeLabel('material');?>: <?php echo $building->material;?>; <?php echo $building->getAttributeLabel('floors');?>: <?php echo $building->floors;?></div>
		<!-- 
		<div style="font-size:0.8em;">
			<a href="/images/upload/proektnayadeklaratsia<?php echo $building->id;?>.pdf" title="Проектная декларация на <?php echo CHtml::encode($building->name.', '.$building->adress);?>" target="projectdclr">Проектная декларация</a>
			
			&nbsp; &nbsp; <a href="/images/upload/ddu.pdf" title="Договоры долевого участия <?php echo CHtml::encode($building->name.', '.$building->adress);?>" target="ddu">Договоры долевого участия</a>
			&nbsp; &nbsp; <a href="/images/upload/stroy.pdf" title="Разрешение на строительство <?php echo CHtml::encode($building->name.', '.$building->adress);?>" target="stroi">Разрешение на строительство</a>
					
		</div>
		-->		
	</div>
	
	<table class="b-table_std" id="flatTable" style="margin-bottom:1em;">
	<thead>
	<tr>
	<td></td><td>Этажи</td><td>Общая <br>площадь</td><td>Кухня</td><td>Цена</td><td></td><td></td><td></td>
	</tr>
	</thead>
	<tbody>
	<?php	
	$flats=$building->flatsGroup;	
	
	$ctflats=count($flats);
	
	if (!($ctflats>0)) {
		?>
		<tr  class="e-hrow"><td colspan=10 >Квартир в продаже в данный момент нет</td></tr>
		<?php 
	}
	
	$old_rooms=-1;
	foreach ($flats as $flat) {
	    
		$imgTitle=$flat['plan']->name.', '.$complexName.', '.$building->name.', цена '.TextHelper::price($flat['price']).' руб';
		
		if ($old_rooms<>$flat['plan']->rooms) {
			?>
			</tbody>
			<tbody><tr  class="e-hrow"><td colspan=10 ><?php if ($flat['plan']->rooms>0) { echo $flat['plan']->rooms; ?>-комнатные квартиры<?php } else { ?>Студии<?php } ?></td></tr>
			<?php 
			$old_rooms=$flat['plan']->rooms;
		}
		?>
		<tr>
		  <td><a href="<?php echo $flat['plan']->imgSrc;?>" class="e-grey <?php echo $lightbox;?>" rel="lightbox[plan<?php echo $flat['plan']->id;?>]" title="<?php echo $imgTitle;?>"><?php echo $flat['plan']->name;?></a></td>
		  <td><?php
			
		  	if (count($flat['floors'])>1) {
			  	$floor_l = -999;
				$floor_i = 0;
				
				foreach ($flat['floors'] as $floor) {
					if ($floor <> ($floor_l+1)) {
						if ($floor_i>0) { echo '-'.$floor_l.', '; }
			  			echo $floor;
			  		}
			  		$floor_l = $floor;
			  		$floor_i++;
				}
				
				echo '-'.$floor_l;
		  	} else {		  		
		  		echo array_shift($flat['floors']);
		  	}
		  
		  ?></td>
		  <td><?php echo $flat['plan']->s_total;?> м<sup>2</sup></td>		 
		  <td><?php if ($flat['plan']->s_kitchen>0) { echo $flat['plan']->s_kitchen;?> м<sup>2</sup><?php } ?></td>		 
		  <td><?php echo TextHelper::price($flat['price']);?></td>
		  <td><?php if ($flat['hot']) { ?><a href="<?php echo $this->createUrl('orders/flat',array('flatid'=>$flat['id']));?>" onclick="get_url_to_modal_windows(this.href); return false; " class="b-but" title="Последние квартиры по распродаже!" style="font-weight:normal;">Распродажа!</a><?php } ?></td>
		  <td><a href="<?php echo $flat['plan']->imgSrc;?>" class="e-grey lightbox" title="<?php echo $imgTitle;?>">Планировка</a></td>
		  <td><a href="<?php echo $this->createUrl('orders/flat',array('flatid'=>$flat['id']));?>" class="b-but e-but_grey" onclick="get_url_to_modal_windows(this.href); return false; ">Забронировать!</a></td>
		</tr>
		<?php
	}
	?>
	</tbody>
	</table>
	<?php	
}