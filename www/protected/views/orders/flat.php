<h1>Забронировать квартиру</h1>

<p style="font-size:0.8em;">Внимание! После подтверждения заявки бронь сохраняется только в течении 5 дней!</p>

<div class="b-form" >

<?php 
$model=new Orders;

$model->attributes=array('tema'=>'Бронирование квартиры','flatid'=>$flat->id);


$form=$this->beginWidget('CActiveForm', array(
			'id'=>'order-form4',
			'action'=>$this->createUrl('orders/add'),
			'enableAjaxValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,),
	)
	);


echo $form->hiddenField($model,'tema');


	if (count($floors)>1) {
	?>
	<div class="e-row">
		
		Выберете этаж:
		<span class="e-val" >
		<?php 
		echo $form->dropDownList($model,'flatid',  $floors);
		?>		
		</span>
	</div>
	<?php 
	} else {
		echo $form->hiddenField($model,'flatid');
	}
	?>

	<div class="e-row">
		<?php echo $model->getAttributeLabel('flatid');?>:
		
		<div class="e-val">
						
			<div class="e-field">
				<?php 
				
				if ($flat)
				{
					$building=$flat->plan->building;
					?>
					<div><?php echo $building->adress;?>, <?php echo $building->section->name;?>, <?php echo $building->name;?></div>
					<div style="padding:0.5em 0;"><?php echo $flat->plan->name;?>, <?php if (count($floors)<2) { ?>этаж: <?php echo $flat->floor;?>, <?php } ?>пл:&nbsp;<?php echo $flat->plan->s_total;?> м<sup>2</sup><?php if ($flat->plan->s_kitchen) { ?>, кух <?php echo $flat->plan->s_kitchen;?> м<sup>2</sup><?php } ?></div>
					<div>Цена: <strong><?php echo TextHelper::price($flat->price);?></strong> руб.</div>
					<?php 					
				}
				
				?>
			
			</div>
		
		</div>
	</div>


	<div class="e-row">
		<?php echo $form->error($model,'name'); ?>
		<div class="e-val"><?php echo $form->textField($model,'name',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('name'))); ?></div>
	</div>
	
	
	<div class="e-row">
		<?php echo $form->error($model,'tel'); ?>
		<?php echo $form->textField($model,'tel',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('tel'))); ?>
		
	</div>

	
	<div><?php echo CHtml::submitButton('Забронировать',array('class'=>'b-but'));?></div>
	
	<?php $this->renderPartial('//privacy/_privacy'); ?>

<?php $this->endWidget(); ?>

</div>