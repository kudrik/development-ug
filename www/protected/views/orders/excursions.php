<h1>Запись на экскурсию</h1>

<p>Если Вы хотите увидеть жилые комплексы своими глазами - запишитесь на экскурсию!</p>

<div class="b-form" >

<?php 
$model=new Orders;

$model->attributes=array('tema'=>'Экскурсия');


$form=$this->beginWidget('CActiveForm', array(
			'id'=>'order-form2',
			'action'=>$this->createUrl('orders/add'),
			'enableAjaxValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,),
	)
	);


echo $form->hiddenField($model,'tema');

?>

	<div class="e-row">		
		<div class="e-val"><?php echo $form->textField($model,'dateExcur',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('dateExcur'))); ?></div>
	</div>


	<div class="e-row">
		<?php echo $form->error($model,'name'); ?>
		<div class="e-val"><?php echo $form->textField($model,'name',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('name'))); ?></div>
	</div>
	
	
	<div class="e-row">
		<?php echo $form->error($model,'tel'); ?>
		<?php echo $form->textField($model,'tel',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('tel'))); ?>
		
	</div>

	
	<div><?php echo CHtml::submitButton('Записаться',array('class'=>'b-but'));?></div>
	
	<?php $this->renderPartial('//privacy/_privacy'); ?>

<?php $this->endWidget(); ?>

</div>