<table>
<?php

foreach ($model->attributes as $k=>$val)
{
	if ($k=='date')  { $val=date('d.m.Y H:i',$val); }
	
	if ($k=='flatid') { continue; }
	
	?>
<tr><td><?php echo $model->getAttributeLabel($k);?></td><td><?php echo $val;?></td></tr>
<?php 
}

if ($flat=$model->flat)
{
	?>
	<tr><td>Квартира</td><td><?php echo $flat->plan->building->name;?>, <?php echo $flat->plan->name;?>, <?php echo $flat->plan->s_total;?>/<?php echo $flat->plan->s_living;?>/<?php echo $flat->plan->s_kitchen;?>, этаж: <?php echo $flat->floor;?>, подъезд: <?php echo $flat->entrances;?>, цена: <?php echo TextHelper::price($flat->price);?> руб., Планировка <a class="lightbox" href="<?php echo Yii::app()->getBaseUrl(true);?><?php echo $flat->plan->imgSrc;?>"><img src="<?php echo Yii::app()->getBaseUrl(true);?><?php echo $flat->plan->imgPreviewSrc;?>"></a></td></tr>
	<?php 
}
?>
</table>
<?php 