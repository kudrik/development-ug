<?php

/**
 * This is the model class for table "tbl_news".
 *
 * The followings are the available columns in table 'tbl_news':
 * @property integer $id
 * @property integer $date
 * @property string $name
 * @property string $anons
 * @property string $tekst
 * @property string $meta_title
 * @property string $meta_desc
 * @property string $meta_key
 * @property string $img
 */
class Foto extends CActiveRecord
{

    public $addImage;

    /**
     *
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{foto}}';
    }

    public function defaultScope()
    {
        return array(
            'order' => 'sort ASC'
        );
    }

    /**
     *
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name','length','max' => 255),
            array('pid,sort','numerical','integerOnly' => true),
            array('sort','default','value' => time()),
            array('addImage','file','types' => 'jpg, gif, png','on' => 'insert'),
            array('addImage','file','types' => 'jpg, gif, png','allowEmpty' => true)
        );
    }

    /**
     *
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'section' => array(
                self::BELONGS_TO,
                'Section',
                'pid'
            )
        );
    }

    /**
     *
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'addImage' => 'Фото'
        );
    }

    public function getImgPreviewHtml()
    {
        return '<img src="/images/foto/s' . $this->id . '.jpg" alt="">';
    }

    public function getImgPreview()
    {
        return '/images/foto/s' . $this->id . '.jpg';
    }
    
    public function getImgM()
    {
    	return '/images/foto/m' . $this->id . '.jpg';
    }

    public function getImg()
    {
        return '/images/foto/' . $this->id . '.jpg';
    }

    public function search()
    {
        $criteria = new CDbCriteria();
        
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100
            )
        ));
    }

    protected function afterSave()
    {
        
        // грузим картинку
        if ($this->addImage) {
            $imgName = $this->id . '.jpg';
            
            return Yii::app()->ih->load($this->addImage)
                ->thumb(1024, 768)
                ->save(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . 'l'.$imgName, false, 95)
                ->reload()
                ->thumb(800, 600)
                ->save(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . $imgName, false, 95)
                ->reload()
                ->thumb(640, 480)
                ->save(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . 'm' . $imgName, false, 91)           
            	->reload()
            	->thumb(320, 240)
            	->save(Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR . 's' . $imgName, false, 91);
		}
    }

    protected function beforeDelete()
    {
        if ($this->id > 0) {
            
            $imgName = $this->id . '.jpg';
            $dir = Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'foto' . DIRECTORY_SEPARATOR;
            @unlink($dir . $imgName);
            @unlink($dir . 'l' . $imgName);
            @unlink($dir . 'm' . $imgName);
            @unlink($dir . 's' . $imgName);
        }
        
        return true;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * 
     * @param string $className
     *            active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
