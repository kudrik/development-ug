<?php

class Flats extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	 
	public $buildingid;
	public $rooms;
	 
	public function tableName()
	{
		return '{{flats}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('planid,price', 'required'),
			array('planid,price', 'numerical', 'integerOnly'=>true,'min'=>1),
			array('entrances,floor', 'numerical', 'integerOnly'=>true),
			array('price','numerical'),
			array('hot', 'numerical'),			
			array('id, buildingid, rooms, price, hot, top', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(			
			'plan'=>array(self::BELONGS_TO, 'Plans', 'planid'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'building'=>'Дом',
			'floor'=>'Этаж',
			'entrances'=>'Подьезд',
			'planid'=>'Планировка',
			'price' => 'Цена',
			'hot' => 'Скидка/акция',
		    'top' => 'Спецпредложение',
			'rooms'=>'Кол-во комнат',
			'buildingid'=>'Дом',			
		);
	}
		
	public function search()
    {
    	$criteria=new CDbCriteria;
       	
       	$criteria->with=array('plan'=>array('joinType'=>'INNER JOIN'));
		$criteria->together = true;
			
    
    	$criteria->compare('id',$this->id);
    	$criteria->compare('price',$this->price); 
    	$criteria->compare('floor',$this->floor); 
    	$criteria->compare('hot',$this->hot); 
    	$criteria->compare('rooms',$this->rooms);
		$criteria->compare('buildingid',$this->buildingid);
    	
		//$criteria->compare('price',$this->building);
    	/*
    	
    	$criteria->compare('s_total',$this->s_total);
    	$criteria->compare('s_living',$this->s_living);
    	$criteria->compare('s_kitchen',$this->s_kitchen);
    	$criteria->compare('s_loggia',$this->s_loggia);
    	$criteria->compare('loggia',$this->loggia);
    	$criteria->compare('buildingid',$this->buildingid);
    	
    	*/

    	   
    	return new CActiveDataProvider($this, array(
    			'criteria'=>$criteria,
    			//'sort'=>array('defaultOrder'=>'id ASC'),
    			'pagination'    => array('pageSize'  => 100)
    	));
    }
    
    public function getHotValues()
    {
        return array(
            array('id'=>0,'name'=>'Нет'),
            array('id'=>1,'name'=>'Скидка'),
            array('id'=>2,'name'=>'Спецпредложение'),
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatalogSection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
