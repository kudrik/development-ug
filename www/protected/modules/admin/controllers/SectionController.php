<?php

class SectionController extends Controller
{

	public $layout='/layouts/column2';

	
	public function actionIndex()
	{	
		$model=new SectionAdmin('search');	
	
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SectionAdmin']))
			$model->attributes=$_GET['SectionAdmin'];
	
		$this->render('index',array(
				'model'=>$model,
		));
	}


	
	public function actionUpdate($id=0)
	{	
		if (!$model=SectionAdmin::model()->findByPk($id))
		{
			$model=new SectionAdmin;
		}
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SectionAdmin']))
		{
			$model->attributes=$_POST['SectionAdmin'];
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('form',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		
		$this->loadModel($id)->deleteNode();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin/'));
	}



	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SectionAdmin the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SectionAdmin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SectionAdmin $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
