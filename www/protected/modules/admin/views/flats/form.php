<?php
	if ($model->id>0)	{ $tit='Редактирование квартиры'; }
	else				{ $tit='Добавить квартиру'; }
	
	
	$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Квартиры'=>array('flats/index'),
		$tit
	);
	
	$this->menu=array(
			array('label'=>'Планировки', 'url'=>array('plans/index')),
			array('label'=>'Добавить планировку', 'url'=>array('plans/update')),
			array('label'=>'Квартиры', 'url'=>array('flats/index')),			
			array('label'=>'Добавить квартиру', 'url'=>array('flats/update')),
	        array('label'=>'Импорт', 	'url'=>array('flats/import')),
	);
?>

<h1><?php echo $tit;?></h1>

<div class="b-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'foto-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
	
)); 

	 echo $form->hiddenField($model,'id');

	 echo $form->errorSummary($model);
	 
	 
	 
	 ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'building'); ?>
		<?php echo $form->dropDownList(
										$model,
										'buildingid',
										CHtml::listData(Buildings::model()->findAll(),'id','name'),
										array(	
												'ajax' => array(											 
													'url'=>$this->createUrl('plans/PlansList'),
													'data'=>array('buildingid'=>'js:$(this).val()','type'=>'radioButtonList','form'=>'Flats'),
													'update'=>'#Flats_planid',
												),
												'empty'=>'',
											)
										); ?>
		
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'planid'); ?>
		<div id="Flats_planid">
			
		<?php
			if (!($model->plan)) { echo 'Для выбора планировок, выберете сначала дом'; }
		
			$this->renderPartial('/plans/_label',array('row'=>$model->plan,'form'=>'Flats'));
		?>
		
		</div>
		<?php echo $form->error($model,'planid'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'floor'); ?>
		<?php echo $form->textField($model,'floor'); ?>
		<?php echo $form->error($model,'floor'); ?>
	</div>
			
	<div class="row">
		<?php echo $form->labelEx($model,'entrances'); ?>
		<?php echo $form->textField($model,'entrances'); ?>
		<?php echo $form->error($model,'entrances'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'hot'); ?>
		<?php echo $form->dropDownList($model,'hot',CHtml::listData($model->getHotValues(),'id','name') ); ?>
		<?php echo $form->error($model,'hot'); ?>
	</div>
	

	<div class="row buttons" style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->




<a name="b-images"></a>
<div class="b-images" name="b-images">


<?php
/*
if (count($model->image)>0)
{
	?><h3>Изображения</h3><?php


	foreach ($model->images as $k=>$img)
	{
		?>
		<div class="e-row">
			<a href="<?php echo $img['big'];?>" target="newimg" ><img src="<?php echo $img['small'];?>" alt=""></a>
			<a href="<?php echo $this->createUrl('foto/deleteImage',array('id'=>$model->id,'i'=>$k));?>">Удалить?</a>
		</div>
			<?php
	}
}
*/
?>
</div>
