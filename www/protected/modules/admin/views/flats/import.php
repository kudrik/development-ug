<h1>Импорт квартир</h1>
<?php

$this->breadcrumbs=array(
    'Разделы сайта'=>array('section/'),
    'Квартиры'=>array('flats/index'),
    'Импорт квартир',
);

?>
<div class="b-form">
<?php 

if (isset($unknownRows) AND count($unknownRows)>0) {
    
    ?>
    <span class="errorMessage">Не вставленные строки:</span>
    <?php 
    
    foreach ($unknownRows as $unknownRow) {
        ?>
        <pre>
        <?php echo implode(';',$unknownRow); ?>
        </pre>
        <?php         
    }
}

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'import-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
	
));
?>


	<div class="row">
        <?php echo $form->labelEx($model,'file'); ?>        
        <?php echo $form->fileField($model,'file'); ?>
        <?php echo $form->error($model,'file'); ?>
    </div>
    
    <div class="e-row">
		<?php echo $form->checkBox($model,'replacement'); ?>
		<?php echo $form->label($model,'replacement'); ?>
		<?php echo $form->error($model,'replacement'); ?>
	</div>
    

	<div class="row buttons" style="margin-top:20px;">
		<?php echo CHtml::submitButton('Загрузить'); ?>
	</div>

<?php $this->endWidget(); ?>
</div>