<?php
/* @var $this SectionController */
/* @var $model Section */

$this->breadcrumbs=array(
	'Разделы сайта'
);

$this->menu=array(
	array('label'=>'Список разделов', 'url'=>array('index')),
	array('label'=>'Создать раздел', 'url'=>array('update')),
);

?>

<h1>Управление разделами</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'section-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		array('name'=>'name','value'=>'$data->nameWithLevelGV','type'=>'html'),
		
		'url',			
		
		array(
			'class'=>'CButtonColumn',
				
			'buttons'=>array(

				'catalog'=>array(					
					'label'=>'Каталог товаров',
					'url'=>'Yii::app()->createUrl("admin/catalog", array("id"=>$data->id))',
					'imageUrl'=>'/images/admin/catalog.png',
					'visible'=>'$data->getModules()=="catalog"',
				),
					
				'shop'=>array(
							'label'=>'Магазин',
							'url'=>'Yii::app()->createUrl("admin/shop", array("id"=>$data->id))',
							'imageUrl'=>'/images/admin/catalog.png',
							'visible'=>'$data->getModules()=="shop"',
					),
					
				'foto'=>array(
							'label'=>'Фотогаллерея',
							'url'=>'Yii::app()->createUrl("admin/foto", array("id"=>$data->id))',
							'imageUrl'=>'/images/admin/foto.png',
							'visible'=>'$data->getModules()=="foto"',
					),
			
				'news'=>array(								
					'label'=>'Новости',
					'url'=>'Yii::app()->createUrl("admin/news", array("id"=>$data->id))',
					'imageUrl'=>'/images/admin/news.png',
					'visible'=>'$data->getModules()=="news"',
				),				
					
				'partners'=>array(
							'label'=>'Партнеры',
							'url'=>'Yii::app()->createUrl("admin/partners", array("id"=>$data->id))',
							'imageUrl'=>'/images/admin/partners.png',
							'visible'=>'$data->getModules()=="partners"',
					),
					
				
			),
										
				
			'template'=>'{partners} {news} {catalog} {shop} {foto} {update} {delete}',
		),
	),
));
