<?php
	if ($model->id>0)	{ $tit='Редактирование планировки'; }
	else				{ $tit='Добавить планировку'; }
	
	
	$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Планировки'=>array('plans/index'),
		$tit
	);
	
	$this->menu=array(
			array('label'=>'Планировки', 			'url'=>array('plans/index')),
			array('label'=>'Добавить планировку',	'url'=>array('plans/update')),
			array('label'=>'Квартиры', 				'url'=>array('flats/index')),			
			array('label'=>'Добавить квартиру', 	'url'=>array('flats/update')),
	        array('label'=>'Импорт', 	            'url'=>array('flats/import')),
	);
?>

<h1><?php echo $tit;?></h1>

<div class="b-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'foto-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
	
)); 

	 echo $form->hiddenField($model,'id');

	 echo $form->errorSummary($model); ?>

	
	<div class="row">
		<?php echo $form->labelEx($model,'buildingid'); ?>
		<?php echo $form->dropDownList($model,'buildingid',CHtml::listData(Buildings::model()->findAll(),'id','name') ); ?>
		<?php echo $form->error($model,'buildingid'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'rooms'); ?>
		<?php echo $form->textField($model,'rooms'); ?>
		<?php echo $form->error($model,'rooms'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'s_total'); ?>
		<?php echo $form->textField($model,'s_total'); ?>
		<?php echo $form->error($model,'s_total'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'s_living'); ?>
		<?php echo $form->textField($model,'s_living'); ?>
		<?php echo $form->error($model,'s_living'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'s_kitchen'); ?>
		<?php echo $form->textField($model,'s_kitchen'); ?>
		<?php echo $form->error($model,'s_kitchen'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'loggia'); ?>
		<?php echo $form->checkBox($model,'loggia'); ?>
		<?php echo $form->error($model,'loggia'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'s_loggia'); ?>
		<?php echo $form->textField($model,'s_loggia'); ?>
		<?php echo $form->error($model,'s_loggia'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'addImage'); ?>        
        <?php echo $form->fileField($model,'addImage'); ?>
        <?php echo $form->error($model,'addImage'); ?>
    </div>

	 <div class="row">
		<?php echo $form->labelEx($model,'tekst'); ?>
		<?php echo $form->textField($model,'tekst'); ?>
		<?php echo $form->error($model,'tekst'); ?>
	</div>

	<div class="row buttons" style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<?php
if ($model->id>0)
{
	?>
	<h3>Изображение</h3>
	<img src="<?php echo $model->imgSrc;?>" alt="" style="max-width:100%;">
	<?php
	
}


