<h1>Заказы</h1>
<?php
$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Заказы'
);



$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orders-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		'id',
				
		array(
			'name'=>'date',			
			'value'=>'date("d.m.Y",$data->date)',
			'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			           'model'=>$model,
			           'attribute'=>'date',
			           'language'=>'ru',
			           'options'=>array(
							'showAnim'=>'fold',
							'dateFormat'=>'dd.mm.yy',
							'changeMonth' => 'true',
							'changeYear'=>'true',
						),
				),true),
		),
		
		'name',
		
		'tel',
		
		'tema',
		
				
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
		),
	),
));
