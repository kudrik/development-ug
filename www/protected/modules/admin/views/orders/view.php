<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Заказы'=>array('orders/'),
		'Редактирование заказа #'.$model->id,
);

$this->menu=array(
		array('label'=>'Список заказов', 'url'=>array('index')),
		
		array('label'=>'Добавить позицию в заказ #'.$model->id, 'url'=>array('orders/updateItem','orderid'=>$model->id)),
);


?>

<h1>Заказ #<?php echo $model->id;?> от <?php echo date('d.m.Y',$model->date);?></h1>

<div class="b-user_info">


	  	<?php
	  	
		
		$this->renderPartial('//orders/order',array('model'=>$model));
		
		
		?>
	  
		

	
	
	
	
</div>