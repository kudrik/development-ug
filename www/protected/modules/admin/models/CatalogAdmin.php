<?php
class CatalogAdmin extends Catalog
{
	//для формы редактирования комплектации в заказе
	public function getTreeBySections()
	{
		$m=array();	
		
		//бежим по категориям
		foreach (Section::model()->findByPk(Yii::app()->params['catalog_section_id'])->descendants()->findAll() as $section)
		{			
			
			
			$m[$section->name]=array();			
			
			
			//выбираем товары из каждой категории присоединяя комплектации
			foreach (Catalog::model()->with(
						array(						
							'catalogSection'=>array(
								'joinType'=>'INNER JOIN',
								'condition'=>'sectionid='.$section->id
								))						
						
						)->findAll() as $catalog)
			{
				$m[$section->name][$catalog->id]=$catalog->name.' ('.$catalog->artikul.', '.$catalog->price.' руб.)';				
			}						
		}	
		
		return $m;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}