$(document).ready(function(){
		
	$(window).load(function() {
		
		var $root = $('html, body');
		$('a').click(function() {
			
			var href=$.attr(this, 'href');			
			
			if (href.indexOf('#')>-1 && (href.substr(0,1)=='#' || href.substr(1,1)=='#')) {

				var hrefSplit=href.split('#');
				
				if (hrefSplit[1]) {
					
					$root.animate({
				        scrollTop: $('#'+hrefSplit[1] ).offset().top
				    }, 1000);
					return false;
				}
			}
		    
		});
		
		var backToTop = $('.b-backToTop');
		
		$(window).on('scroll', function () {			
			
            	if ($(this).scrollTop() > 400) {
            		backToTop.fadeIn();
            	} else {
            		backToTop.fadeOut();
            	}
        	});

        backToTop.click(function() {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });
		
	});
});

function modal_window(id,position)
{
	//Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	
		
	if (!($('#mask').length>0))
	{
		 $("body").append('<div style="position:absolute;left:0;top:0;z-index:9000;background-color:#000;display:none;" id="mask"></div>');
	}
	
	
	//Set heigth and width to mask to fill up the whole screen
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	
	//transition effect		
	$('#mask').fadeIn(500);	
	$('#mask').fadeTo("fast",0.9);	

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();
    
		
	//Set the popup window to center
	if (position=='center') {			
		$(id).css('top',winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
		$(id).css('position','fixed');
	}
	
	$(id).css('zIndex',9003);	
	
	//transition effect
	$(id).fadeIn(1000);
	
	//if close button is clicked
	$(id).find('.b-close').click(function (e) {		
		$('#mask').remove();
		$(id).remove();
		$('.window').remove();
		return false;
	});			
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).remove();		
		$(id).remove();
		$('.window').remove();
	});
}


function get_url_to_modal_windows(url)
{
	var divid = 'mw'+new Date().getTime();
	
	$.get(url,'',function(data) {
		$("body").append('<div id="'+divid+'"  class="b-modal_shell">'+data+'</div>');
		modal_window('#'+divid,'center',true);
	});
}


function AjaxFormRequest(form_id,result_id,type_return) {
    
    
    var form_obj=$("#"+form_id);        
    
	$.ajax({
		url:	form_obj.attr('action'), //Адрес подгружаемой страницы
		type:	form_obj.attr('method'), //Тип запроса
		dataType: type_return, //Тип данных
		data: form_obj.serialize()+'&js=1', 
		success: function(response) { 
        	
        	
        	
        	if (type_return!='script')
        	{
        		//Если все нормально
        		$("#"+result_id).html(response);
        	}
		},
		error: function(response)
		{
			if (type_return!='script')
        	{
				$("#"+result_id).html("Ошибка при отправке данных");
			}
			else
			{
				alert("Ошибка при отправке данных");
			}
        }
	});
	 

}


function tableExpander(selector,limit,text)
{
	
	
	var objTrs=$(selector+" tr");
	
	objTrs.each(function() { 
		
			var obj=$(this);
		
			if (obj.index()>limit)
			{
				if (obj.index()==(limit+1))
				{
					//сколько колонок
					var colspan=obj.find('td').length;
					
					//запоминаю парента
					var parent=obj.parent();
					
					var link=$('<tr><td colspan='+colspan+'><a href="#" >'+text+'</a></td></tr>');
					
					link.click(function() {  parent.find('tr').each(function() { $(this).show(); }); $(this).hide(); return false;  });
					
					obj.before(link);
				}
				
				obj.hide();
			}
			
			
			});
}

function yaMapInit(){	
	
    var myMap = new ymaps.Map('map', {
        center: [54.939174, 82.962428],
        zoom: 14					
    });

	myMap.controls.remove('searchControl');
	myMap.controls.remove('trafficControl');
	
	
	var myRectangle = new ymaps.Rectangle(
								[[54.938987, 82.960047],[54.938025, 82.963999]],
								{ balloonContent: 'Жилой комплекс Акварельный' },
								{  fillOpacity:0.5 }
							);
	
	myMap.geoObjects.add(myRectangle);
	
	var balloon = new ymaps.Balloon(myMap);
	
	balloon.options.setParent(myMap.options);
	
	balloon.open([54.938774, 82.962428],'ЖК Акварельный');
	
	

}